package com.gui.common.aspect;

import com.gui.common.ann.Retry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.annotation.Priority;

@Component
@Aspect
@Priority(Integer.MAX_VALUE)
@Slf4j
public class RetryAspect {

    private static final long MAX_WAIT = 1;

    private static final long MIN_WAIT = 6;

    @Around(value = "@annotation(retry) && execution(* *..*(..))")
    public Object query(ProceedingJoinPoint jp, Retry retry) throws Throwable {
        log.debug("{}", jp.getThis());
        int count = retry.value();
        final Object[] args = jp.getArgs();
        Object retVal = null;
        do {
            try {
                retVal = runMethod(jp, args);
                count = 0;
            } catch (Exception e) {
                log.error("failed to exec {} retry {}", jp.getSignature(), e, count);
                count--;
                if (count > 0) {
                    Thread.sleep(RandomUtils.nextLong(MIN_WAIT, MAX_WAIT) * 100);
                } else {
                    throw e;
                }
            }
        } while (count > 0);
        return retVal;
    }

    private Object runMethod(ProceedingJoinPoint joinPoint, Object[] args) throws Throwable {
        Object result;
        if (args == null || args.length == 0) {
            result = joinPoint.proceed();
        } else {
            result = joinPoint.proceed(args);
        }
        return result;
    }
}
