package com.gui.shop.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;


@Data
@Entity
@Table(name = "t_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(name = "main_category")
    private Long mainCategory;

    @Column(scale = 2, precision = 10)
    private BigDecimal price;

    @Column(length = 3)
    private String currency;
}
