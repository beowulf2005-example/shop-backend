package com.gui.shop.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;

@Slf4j
public final class ServiceUtils {

    public ServiceUtils() {

    }

    public static <T> void merge(T target, @Nonnull Object ...source){
        for (Object s:source){
            try {
                PropertyUtils.copyProperties(target,s);
            } catch (IllegalAccessException|InvocationTargetException|NoSuchMethodException e) {
                log.error("failed to copy properties from {} to {}",s,target);
            }
        }
    }
}
