package com.gui.shop.service;

import com.gui.shop.model.Category;
import com.gui.shop.repos.CategoryRepository;
import com.gui.shop.util.ServiceUtils;
import com.gui.shop.web.query.CategoryQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Slf4j
public class CategoryService {

    @Inject
    private CategoryRepository categoryRepository;

    public Category findById(Long id) {
        return categoryRepository.findById(id).orElse(null);
    }

    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    public Category replace(Long id, Category category) {
        //check whether old one exists.
        boolean exist = categoryRepository.existsById(id);
        if (exist) {
            return categoryRepository.save(category);
        } else {
            return null;
        }
    }

    public Category modify(Long id, Category category) {
        Category cate = categoryRepository.findById(id).orElse(null);
        if (cate != null) {
            //object merge
            ServiceUtils.merge(cate, category);
            return categoryRepository.save(cate);
        } else {
            return null;
        }
    }

    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }

    public List<Category> query(CategoryQuery query) {
        return categoryRepository.findByParent(query.getCategoryId());
    }
}
