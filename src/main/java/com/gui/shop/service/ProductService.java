package com.gui.shop.service;

import com.gui.shop.model.Product;
import com.gui.shop.repos.ProductRepository;
import com.gui.shop.util.ServiceUtils;
import com.gui.shop.web.query.ProductQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Slf4j
public class ProductService {

    @Inject
    private ProductRepository productRepository;

    public Product findById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public Product replace(Long id, Product product) {
        //check whether old one exists.
        boolean exist = productRepository.existsById(id);
        if (exist) {
            return productRepository.save(product);
        } else {
            return null;
        }
    }

    public Product modify(Long id, Product product) {
        Product prod = productRepository.findById(id).orElse(null);
        if (prod!=null){
            //object merge
            ServiceUtils.merge(prod, product);
            return productRepository.save(prod);
        }else {
            return null;
        }
    }

    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    public List<Product> query(ProductQuery query) {
        return productRepository.findByMainCategory(query.getCategoryId());
    }
}
