package com.gui.shop.web.controller;

import com.gui.shop.model.Category;
import com.gui.shop.service.CategoryService;
import com.gui.shop.web.query.CategoryQuery;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@Api("category")
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {

    @Inject
    private CategoryService categoryService;

    @ApiOperation(value = "create category",
            notes = "create new category. ",
            response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully.")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Category create(
            @ApiParam(value = "category to create", required = true)
            @Valid @RequestBody Category category) {
        log.debug("create category", category);
        return categoryService.save(category);
    }

    @ApiOperation(value = "find category",
            notes = "find category by Id",
            response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "category with given Id")
    })
    @GetMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Category read(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        return categoryService.findById(id);
    }

    @ApiOperation(value = "update category",
            notes = "update category via replace",
            response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "the updated category")
    })
    @PutMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Category update(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "category to update", required = true)
            @Valid @RequestBody Category category) {
        return categoryService.replace(id, category);
    }

    @ApiOperation(value = "patch category",
            notes = "partial update category",
            response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "the updated category")
    })
    @PatchMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Category patch(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "partial category", required = true)
            @Valid @RequestBody Category category) {
        return categoryService.modify(id, category);
    }

    @ApiOperation(value = "delete category by id",
            notes = "delete category by id",
            response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "deleted category")
    })
    @DeleteMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        categoryService.deleteById(id);
    }

    @ApiOperation(value = "query",
            notes = "query category",
            response = Category.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "a list of category, that match the criterion")
    })
    @PostMapping(value="/query",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> query(
            @ApiParam(value = "criterion", required = true)
            @Valid @RequestBody CategoryQuery categoryQuery) {
        return categoryService.query(categoryQuery);
    }
}
