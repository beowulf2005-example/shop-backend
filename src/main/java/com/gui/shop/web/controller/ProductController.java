package com.gui.shop.web.controller;

import com.gui.shop.model.Product;
import com.gui.shop.service.ProductService;
import com.gui.shop.web.query.ProductQuery;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@Api("product")
@RestController
@RequestMapping("/product")
@Slf4j
public class ProductController {

    @Inject
    private ProductService productService;

    @ApiOperation(value = "create product",
            notes = "create new product. The id of Permission MUST be null, otherwise the system will just ignore it without warning.",
            response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product in response body are successfully created.")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Product create(
            @ApiParam(value = "product to created", required = true)
            @Valid @RequestBody Product product) {
        log.debug("create product", product);
        return productService.save(product);
    }

    @ApiOperation(value = "read product",
            notes = "find product by id",
            response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "product with given id")
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product read(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        return productService.findById(id);
    }

    @ApiOperation(value = "update product",
            notes = "update product through replace",
            response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "updated product")
    })
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product update(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "product with new values to update", required = true)
            @Valid @RequestBody Product product) {
        return productService.replace(id, product);
    }

    @ApiOperation(value = "patch product",
            notes = "partial update",
            response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "updated product")
    })
    @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product patch(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "partial product to update", required = true)
            @Valid @RequestBody Product product) {
        return productService.modify(id, product);
    }

    @ApiOperation(value = "delete product",
            notes = "delete product by id",
            response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful")
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(
            @ApiParam(value = "id", required = true)
            @PathVariable("id") Long id) {
        productService.deleteById(id);
    }

    @ApiOperation(value = "query product",
            notes = "query product in category",
            response = Product.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "a list of product")
    })
    @PostMapping(value = "/query", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> query(
            @ApiParam(value = "criterion", required = true)
            @Valid @RequestBody ProductQuery productQuery) {
        return productService.query(productQuery);
    }
}
