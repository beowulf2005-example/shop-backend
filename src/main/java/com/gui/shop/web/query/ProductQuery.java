package com.gui.shop.web.query;

import lombok.Data;

@Data
public class ProductQuery {
    Long categoryId;
}
