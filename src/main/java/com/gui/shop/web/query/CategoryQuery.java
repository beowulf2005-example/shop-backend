package com.gui.shop.web.query;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CategoryQuery {
    Long categoryId;
}
