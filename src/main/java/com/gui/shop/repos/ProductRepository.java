package com.gui.shop.repos;

import com.gui.shop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByMainCategory(Long mainCategory);
}
