package com.gui.shop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtils {

    public static final ObjectMapper MAPPER = new ObjectMapper();

    public static String toJSON(Object o) {
        try {
            return MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Object to JSON Error. " + e.getMessage();
        }
    }
}
